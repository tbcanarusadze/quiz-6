package com.example.myapplication

import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val requestCode1 = 10
    private val userList = mutableListOf<User>()
    private lateinit var adapter: RecyclerViewAdapter
    private val position = 0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        adapter = RecyclerViewAdapter(userList, object: ItemOnClick{
            override fun onClickDelete(position: Int) {

            }

            override fun onClickEdit(position: Int) {

            }
        })
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        addButton.setOnClickListener {
            val intent = Intent(this, UserActivity::class.java)
            intent.putExtra("position", position)
            startActivityForResult(intent, requestCode1)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        //when we want to add an item
//        if (resultCode == Activity.RESULT_OK && requestCode == requestCode1) {
//            val user = data?.extras?.getParcelable<User>("userModelOutPut")
//            val position  = data?.extras?.get("position") as Int
//            userList.add(0, User(1,user.title, user.description, user.picture))
//            adapter.notifyItemInserted(position)
//            recyclerView.scrollToPosition(position)
        emptyTextView.visibility = View.GONE
        
        





}}
