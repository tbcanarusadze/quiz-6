package com.example.myapplication

import android.content.Intent
import android.view.View
import kotlinx.android.synthetic.main.user_recycler_layout.*
import kotlinx.android.synthetic.main.user_recycler_layout.view.*
//
interface ItemOnClick {

    fun onClickDelete(position: Int)
    fun onClickEdit(position: Int)
//    fun onClick(position: Int)

}