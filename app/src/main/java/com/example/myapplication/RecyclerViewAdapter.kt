package com.example.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.user_recycler_layout.view.*

class RecyclerViewAdapter(
    private val users: MutableList<User>,
    private val itemOnClick: ItemOnClick
) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {

        fun onBind() {
            val model = users[adapterPosition]
            itemView.titleTextView.text = model.title
            itemView.descTextView.text = model.description
            Glide.with(itemView.context).load(model.picture).into(itemView.imageView)
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                R.id.deleteButton -> {
                    itemOnClick.onClickDelete(adapterPosition)
                }
                R.id.editButton -> {
                    itemOnClick.onClickEdit(adapterPosition)
                }
            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.user_recycler_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount() = users.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }


}