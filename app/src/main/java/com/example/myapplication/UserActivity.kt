package com.example.myapplication

import android.app.Activity
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import kotlinx.android.synthetic.main.activity_user.*
import kotlinx.android.synthetic.main.user_recycler_layout.*

class UserActivity : AppCompatActivity() {

    private val db by lazy {
        Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "database-name"
        ).build()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        init()
    }

    private fun init() {
        saveButton.setOnClickListener {
            val title = titleEditText.text.toString()
            val description = descEditText.text.toString()
            if (title.isEmpty() || description.isEmpty()) {
                Toast.makeText(applicationContext, "Please fill all fields!", Toast.LENGTH_SHORT)
                    .show()
            } else if (title.length < 5 || title.length > 30) {
                Toast.makeText(
                    applicationContext,
                    "title min length -5 max length-30",
                    Toast.LENGTH_SHORT
                )
                    .show()
            } else if (description.length < 32 || description.length > 300) {
                Toast.makeText(
                    applicationContext,
                    "desc min length -32 max length-300",
                    Toast.LENGTH_SHORT
                )
                    .show()
            } else saveInfo()


        }
    }

    private fun saveInfo() {
        val user = User(
            1,
            titleEditText.text.toString(),
            descEditText.text.toString(),
            "https://media.macphun.com/img/uploads/customer/how-to/579/15531840725c93b5489d84e9.43781620.jpg?q=85&w=1340"
        )
        val position = intent.extras?.get("position") as Int
//        intent.putExtra("position", position)
//        intent.putExtra("userModelOutPut", user)

        AsyncTask.execute {
            db.userDao().insertAll(
                User(
                    1,
                    titleEditText.text.toString(),
                    descEditText.text.toString(),
                    "https://media.macphun.com/img/uploads/customer/how-to/579/15531840725c93b5489d84e9.43781620.jpg?q=85&w=1340"
                )
            )
        }

//        AsyncTask.execute {
//            for (user in db.userDao().getAll()) {
//                Log.d("userFirstName", "${user.title}")
//            }

        setResult(Activity.RESULT_OK, intent)
        finish()
    }




}

